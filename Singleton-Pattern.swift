import UIKit
class Singleton {
    static let sharedInstance = Singleton() //<- Singleton Instance
    var instanceNum: Int = 0
    
    private init() { /* Additional instances cannot be created */ }
}

/* Test Code */
let instance1 = Singleton.sharedInstance
let instance2 = Singleton.sharedInstance

instance1.instanceNum = 1
instance2.instanceNum = 2
assert(instance1.instanceNum == 2)