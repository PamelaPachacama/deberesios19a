struct Orc: Character {
    func getHealth() -> Int {
        return 10
    }
}

struct OrcBarbarian: Character {
    func getHealth() -> Int {
        return 15
    }
}

struct OrcWarlord: Character {
    func getHealth() -> Int {
        return 20
    }
}