class MyObserver : Observer {
    
    private var _id : Int = 0
    
    var id: Int {
        get {
            return self._id
        }
    }
    
    init(id: Int) {
        self._id = id
    }
    
    func update<T>(with newValue: T) {
        print("Whoaa, observer #\(self.id) updated with new value : ", newValue)
    }
}