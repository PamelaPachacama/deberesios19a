var obs1 = MyObserver(id: 1)
var obs2 = MyObserver(id: 2)
var obsv = Variable<String>()
obsv.addObserver(observer: obs1)
obsv.addObserver(observer: obs2)
obsv.value = "Hello world"
obsv.removeObserver(observer: obs1)
obsv.value = "Obs1 removed, yey"