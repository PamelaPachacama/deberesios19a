protocol Observer {
    var id : Int { get } // property to get an id
    func update<T>(with newValue: T)
}